% Welcome to DebConf15
% The DebConf15 Team
% 2015-08-15

Welcome to DebConf
==================

* Welcome to Heidelberg
* Biggest DebConf so far
	* Around 550 registrations
* Website: 
	* http://debconf15.debconf.org/
* Wiki:
	* https://wiki.debconf.org/wiki/DebConf15
	* https://wiki.debconf.org/wiki/DebConf15/FAQs
	* https://wiki.debconf.org/wiki/DebConf15/HouseRules
* WLAN: debconf15* password: debconf15
	* everything except debconf15-nat is directly on the net
* Wear your badge at all times
	* Please do not swap badges during the first days

Venue 
=====

![Venue Overview](images/venue_overview.png)

Venue
=====

* Talk/BoF rooms (video-taped)
	* Heidelberg (This One)
	* Berlin/London (behind the bar)
		* Overflow Room for Plenaries
	* Amsterdam (BoFs, upstairs)
* BoF/Workshop rooms (not video-taped)
	* Helsinki (upstairs)
	* Stockholm (upstairs)
* Hacklabs
	* Under Heidelberg (outside)
	* Outside Lobby Hacklab (blue tables, outside)
	* Paris (no expectation of quietness)
	* We will designate further rooms
	* Check https://wiki.debconf.org/wiki/DebConf15/Hacklabs
	* The whole venue is a hacklab!

Venue
=====

* Meal times
	* Breakfast: 07:00-10:00
	* Lunch: 12:00-14:00
	* Coffe: 15:30-17:00
	* Dinner: 18:30-20:30
* Bar/Bistro is open 12:00-02:00 (depending on demand)
	* We cut prices from DebCamp thanks to a sponsor
* Tap Water
	* Tap water is perfectly drinkable, 
	* Use the water flask in your conference bag

Morning briefings
=================

* Every Morning 9:45
* Important Information
* Raffle

Thanks to sponsors - Platinum
=============================

![Platinum Sponsor](images/sponsors/platinum/hp.png)

Thanks to sponsors - Gold
=========================

<!--
![credativ](images/sponsors/gold/credativ.png)
![Sipgate](images/sponsors/gold/sipgate.png)
![Matanel](images/sponsors/gold/matanel.png)
![IBM](images/sponsors/gold/ibm.png)
![Google](images/sponsors/gold/google.png)
![Valve](images/sponsors/gold/valve.png)
-->

![Gold Sponsors](images/sponsors/gold.png)

Thanks to sponsors - Silver
===========================

<!--
![Farsight Security](images/sponsors/silver/farsight.png)
![Example 42](images/sponsors/silver/example42.png)
![Ubuntu](images/sponsors/silver/ubuntu.png)
![Mirantis](images/sponsors/silver/mirantis.png)
![1&1](images/sponsors/silver/1und1.png)
![MySQL](images/sponsors/silver/mysql.png)
![conova](images/sponsors/silver/conova.png)
![Hudson River Trading](images/sponsors/silver/hudsonriver.png)
![Cumulus Networks](images/sponsors/silver/cumulus.png)
![Fujitsu](images/sponsors/silver/fujitsu.png)
![ARM](images/sponsors/silver/arm.png)
![Two Sigma](images/sponsors/silver/twosigma.png)
![BMW Group](images/sponsors/silver/bmwgroup.png)
-->

![Silver Sponsors](images/sponsors/silver.png)

Thanks to sponsors - Bronze
===========================

<!--
![Godiug](images/sponsors/bronze/godiug.png)
![Logilab](images/sponsors/bronze/logilab.png)
![Netways](images/sponsors/bronze/netways.png)
![Hetzner](images/sponsors/bronze/hetzner.png)
![UZH](images/sponsors/bronze/uzh.png)
![Deduktiva](images/sponsors/bronze/deduktiva.png)
![Docker](images/sponsors/bronze/docker.png)
![DG-i](images/sponsors/bronze/dg-i.png)
![ETH](images/sponsors/bronze/ethz-isgee.png)
![Univention](images/sponsors/bronze/univention.png)
![Dr. Markus Blatt](images/sponsors/bronze/dr-blatt.png)
![Meinberg](images/sponsors/bronze/meinberg.png)
![GUUG](images/sponsors/bronze/guug.png)
![teamix](images/sponsors/bronze/teamix.png)
![PWC](images/sponsors/bronze/pwc.png)
-->

![Bronze Sponsors](images/sponsors/bronze.png)

Thanks to sponsors - Infrastructure
===================================

<!--
![Bytemark Hosting](images/sponsors/infrastructure/bytemark.png)
![man-da](images/sponsors/infrastructure/manda.png)
![Rackspace](images/sponsors/infrastructure/rackspace.png)
![Linode](images/sponsors/infrastructure/linode.png)
![gandi.net](images/sponsors/infrastructure/gandi.png)
![gandi.net](images/sponsors/infrastructure/gandi.png)
![oriented.net](images/sponsors/infrastructure/oriented-net.png)
![GPLHost](images/sponsors/infrastructure/gplhost.png)
![Amara](images/sponsors/infrastructure/amara.png)
-->

![Infrastructure Sponsors](images/sponsors/infrastructure.png)

<div class="notes">
thanks to network-team/video-team
</div>

<div class="notes">
Network
=======

* Uplink is 1GBit fibre via the elephant's enclosure at the zoo
* 33 operating WLAN APs
* 5 server racks in different rooms
* A/C with 6,5 kW in the "server room"
</div>

Open Weekend
============

* Birthday Party
 * This Weekend Debian turns 22!
 * (band playing)
 * Volunteers needed, come to the Under Heidelberg hacklab from 19:00

<div class="notes">
* Ad-hoc talk scheduling procedure
</div>

Open Weekend Job Fair
=====================

<!--
![HP](images/sponsors/platinum/hp.png)
![credativ](images/sponsors/gold/credativ.png)
![conova](images/sponsors/silver/conova.png)
![1&1](images/sponsors/silver/1und1.png)
![Two Sigma](images/sponsors/silver/twosigma.png)
![BMW Group](images/sponsors/silver/bmwgroup.png)
![Mirantis](images/sponsors/silver/mirantis.png)
-->

![Job Fair Companies](images/sponsors/job_fair.png)

Timeline
========

* Saturday:
	* Open Weekend  (All Day)
	* Job Fair (Afternoon)
	* Birthday Party (Evening)
* Sunday:
	* Open Weekend  (All Day)
* Monday:
	* Cheese & Wine Party (Evening)
* Wednesday:
	* Day Trip (Morning/Afternoon)
	* BBQ and Concert (Evening)
* Thursday:
	* Conference Dinner (Evening)
* Saturday:
	* Final day and cleanup

Day Trip
=======

Signup on the wiki: https://wiki.debconf.org/wiki/DebConf15/DayTrip

* Option 1:
	* Visit to Technik Museum Speyer
* Option 2:
	* Boat tour from Heidelberg to Neckarstainach 
	* Visit to Dilberg Castle
* Option 3:
	* Visit to Beer Brewery
* Option 4:
	* Hike

Keysigning Party
================

* sha256 of http://deb.li/dc15ksp
	* 498e afa0 dfe7 fb89 2d7b 6d23 2923 d8b1
	* 8da9 80fa 44c4 c686 ffff 9552 f683 60ef
* No centralized KSP event
	* meet people
	* say "I've verified the KSP file, i'm #<WHATEVER>"
	* MEET PEOPLE!

<div class="notes">
quick introduction by dkg/anibal (if time permits)
</div>

Communication
=============

* Website: https://debconf15.debconf.org
* Schedule: https://summit.debconf.org/debconf15/
* Wiki: https://wiki.debconf.org/wiki/DebConf15
* Announcement List: debconf-announce@lists.debconf.org
* Discussion List: debconf-discuss@lists.debconf.org
* IRC channel: #debconf on irc.debian.org
* Twitter: @debconf #debconf15 #dc15quotes
* sha256 of http://deb.li/dc15ksp
	* 498e afa0 dfe7 fb89 2d7b 6d23 2923 d8b1
	* 8da9 80fa 44c4 c686 ffff 9552 f683 60ef

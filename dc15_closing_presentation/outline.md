% DebConf15 Closing Ceremony
% The DebConf15 Team
% 2015-08-23

Thanks to sponsors - Platinum
=============================

![Platinum Sponsor](images/sponsors/platinum/hp.png)

Thanks to sponsors - Gold
=========================

<!--
![credativ](images/sponsors/gold/credativ.png)
![Sipgate](images/sponsors/gold/sipgate.png)
![Matanel](images/sponsors/gold/matanel.png)
![IBM](images/sponsors/gold/ibm.png)
![Google](images/sponsors/gold/google.png)
![Valve](images/sponsors/gold/valve.png)
-->

![Gold Sponsors](images/sponsors/gold.png)

Thanks to sponsors - Silver
===========================

<!--
![Farsight Security](images/sponsors/silver/farsight.png)
![Example 42](images/sponsors/silver/example42.png)
![Ubuntu](images/sponsors/silver/ubuntu.png)
![Mirantis](images/sponsors/silver/mirantis.png)
![1&1](images/sponsors/silver/1und1.png)
![MySQL](images/sponsors/silver/mysql.png)
![conova](images/sponsors/silver/conova.png)
![Hudson River Trading](images/sponsors/silver/hudsonriver.png)
![Cumulus Networks](images/sponsors/silver/cumulus.png)
![Fujitsu](images/sponsors/silver/fujitsu.png)
![ARM](images/sponsors/silver/arm.png)
![Two Sigma](images/sponsors/silver/twosigma.png)
![BMW Group](images/sponsors/silver/bmwgroup.png)
-->

![Silver Sponsors](images/sponsors/silver.png)

Thanks to sponsors - Bronze
===========================

<!--
![Godiug](images/sponsors/bronze/godiug.png)
![Logilab](images/sponsors/bronze/logilab.png)
![Netways](images/sponsors/bronze/netways.png)
![Hetzner](images/sponsors/bronze/hetzner.png)
![UZH](images/sponsors/bronze/uzh.png)
![Deduktiva](images/sponsors/bronze/deduktiva.png)
![Docker](images/sponsors/bronze/docker.png)
![DG-i](images/sponsors/bronze/dg-i.png)
![ETH](images/sponsors/bronze/ethz-isgee.png)
![Univention](images/sponsors/bronze/univention.png)
![Dr. Markus Blatt](images/sponsors/bronze/dr-blatt.png)
![Meinberg](images/sponsors/bronze/meinberg.png)
![GUUG](images/sponsors/bronze/guug.png)
![teamix](images/sponsors/bronze/teamix.png)
![PWC](images/sponsors/bronze/pwc.png)
![Heroku](images/sponsors/bronze/heroku.png)
-->

![Bronze Sponsors](images/sponsors/bronze.png)

Thanks to sponsors - Infrastructure
===================================

<!--
![Bytemark Hosting](images/sponsors/infrastructure/bytemark.png)
![man-da](images/sponsors/infrastructure/manda.png)
![Rackspace](images/sponsors/infrastructure/rackspace.png)
![Linode](images/sponsors/infrastructure/linode.png)
![gandi.net](images/sponsors/infrastructure/gandi.png)
![gandi.net](images/sponsors/infrastructure/gandi.png)
![oriented.net](images/sponsors/infrastructure/oriented-net.png)
![GPLHost](images/sponsors/infrastructure/gplhost.png)
![Amara](images/sponsors/infrastructure/amara.png)
![Thomas-Krenn](images/sponsors/infrastructure/thomas-krenn.jpg)
![Cisco](images/sponsors/infrastructure/cisco.png)
![BelWü](images/sponsors/infrastructure/belwue.gif)
![Universität Heidelberg](images/sponsors/infrastructure/unihd.jpg)
-->

![Infrastructure Sponsors](images/sponsors/infrastructure.png)


*  Procedures

This year we are conducting the evaluation by email.

In order to keep the mail somewhat sane, please try to follow the
conventions suggested below for subject lines, and to start new
threads as approprite (i.e. send mail to bursaries@debconf.org, rather
than always replying).

It's important to notice that in the attached copy of the sponsorship
worksheet, each person is assigned two referees.  For accomodation I
think we can decide based on one persons feedback, but for travel we
will want two. I didn't check for conflict of interest, so please let
me know.

* Food and Accomodation Sponsorship

We have budgeted for roughly 150 people (first and last night less)
for the Debconf period. We should definitely plan on approving more
than that. My rough guess is 175 is probably safe, given people will
not come because they do not get travel sponsorship or some other
reason.

- Subject: Accomodation: <person>

- Sanity check "Definite Yes" people for accomodation I have
marked as "y" those with a something in the "role" field or those with
a 2 in CDO2015.  If there are any there that you think should not be
sponsorsed for accomodation, please speak up before *a deadline*.
Acceptances can be sent early for these.


- decide the ? and 'n' cases.  I have really only looked at
contributors.debian.org (and dug a little deeper from there).  This
means there are almost certainly people with a convincing argument in
the various text fields currently marked 'n' Please look at all of the
people with
  - budget line main
  - n or ?  in accom
  - your referee number in Ref#1 or Ref#2. Unfortunately to get 2
    pairs of eyes on ever file that means about 10 candidates each.

  - if marked 'n', and you agree, no need to followup.

- Roughly speaking, I would say that someone we approve for
  acommodation sponsorship in this discussion should be currently
  contributing to Debian in some some non-trivial way.

- The message templates are in the same directory. You can see that we
  reminded people that selected: "Without the requested funding, I
  will be absolutely unable to attend DebConf", were reminded that in
  this case the accomodation funding only made sense if they were also
  awarded travel funding.


* DebCamp Food and Accomodation Sponsorship

Among the acommodation = y people, check the debcamp 
plans

  - Subject: DebCamp: <person>

  - Here we are only deciding if someone should be sponsored for 
  accomodation at debcamp. We have a significantly smaller pool
  of money here (the budget tops out at 42 sponsored persons), 
  but also a smaller pool of applicants.

  Please rank the debcamp plan for those with
  - budget line main
  - ApproveAccom = Y
  - your referee number in Ref#1 or Ref#2

  In order to avoid confusion, please state the following:

  - clear reject 
  - marginal reject
  - marginal accept
  - clear accept.

  In general we should look positively on plans for collaboration, but
also be open to plans for individual work where it makes sense.

* Travel Funding
** Preamble
   - In preliminary discussions we agreed that the amount of the
     request would not be a factor in the actual ranking.

   - City and country are included to sanity check the amount. In case
     the amount doesn't seem sane, we will follow up with the applicant
     via email

   - financial need will be combined with this ranking using the scheme
     proposed in mid:87wq3bml6k.fsf@maritornes.cs.unb.ca .

     Basically the idea is that nobody should lose out to someone with
     both a higher "contribution ranking" and a higher level of
     financial need.

** Scale

For convenience we will refer to the score assigned by referees on the
bursaries team a *contribution* score.

*** 5 "must fund"
    - If this person does not attend debconf, there will be significant
      negative impact for DebConf or Debian more generally.
*** 4 "priority funding"
    - There are clear benefits to Debian or to DebConf of 
    this person attending debconf. 
    - This might be an accepted talk that seems particularly
      important.
*** 3 "good initiative"
    - We should fund this person because they propose something
      interesting
    - Alternatively we should fund this person because they bring
      diversity to debian, or are a new contributor.
*** 2 "good record"
    - this person has a strong record of contributing to Debian.
*** 1 "OK"
    - if we have budget, I don't object to funding this request
*** 0 "no"
    Even if we have budget, I think we should not this travel reqest


** Aggregating Scores

- Each applicant will get two initial referees.

- An applicant where the first two scores are at least two apart
  will be assigned a third referee.

- In all cases, the contribution score used is the *median* of the
  scores of however many people are assigned as referees.

** Thresholds

- Anyone with an average contribution score below 1 will be eliminated
  from the ranking process and not funded.

- There will not be an explicit threshold on financial need this year.
  This is because people didn't know in advance, and it seems unfair
  to eliminate them based on a technicality.

* Interpreting the CSV files
** Field interpretation
*** job
    This means they selected "I am applying primarily as a volunteer"
    and selected which team.

*** level of financial need
   - Without the requested funding, I will be absolutely unable to
       	attend DebConf. (3)
   - Without the requested funding, I will have to make financial
     sacrifices to attend DebConf. (2)
   - Without the requested funding, attending DebConf will be
     inconvenient for me. (1)
   - I am not applying based on financial need.  (0)
   - none selected, -1
** Added Fields
*** CDO2015
    activity in 2015 reflected in contributors.debian.org
**** level 2
    - package uploads
    - active on nm.debian.org team
    - at least 8 messages sent to BTS
    - 8 wiki edits
    - active translator (8 messages)
**** level 1
     shows up on CDO
     
*** CDO2014
    - 1 if active in 2014,2015
    - 2 if active after Nov 1 2014
*** Role
    Some kind of well defined delegation or role within debian
    - DSA (delegates)
    - DebConfTeam: Team leads or shadows
*** Budget line
**** main
     - for people with well known contributions to Debian
     - this is intended to cover people doing core debconf organization
**** Conf
     - one the budget this is called something like "DebConf only
       contributors"
**** Invited speakers
     - there is budget for invited speakers
**** GSoC
     - once we know who the gsoc students are, they can get travel
       funding from google
     - we are still on the hook for housing. Although there is also 
       an "Outreach" budget line.
       
*** ApproveAccom
    Approved for accomodation sponsorship for debconf
*** Ref #1, Ref #2
    Assignment of referees to this file for various tasks

#!/usr/bin/python
# Copyright 2015 David Bremner <bremner@debian.org>
# Licensed under the WTFPL v2

import sys
from decimal import *
import argparse
import copy
import csv
import random

class person:
    symmetric=False
    def __init__(self, ident, need, score, cost,approved):
        self.ident = ident
        self.need = need
        self.score = score
        self.cost = cost
        self.approved=approved

    # this function defines if 'self' should be definitely
    # ranked after 'other'
    
    def __lt__(self, other):
        return ((self.need <= other.need) and \
            (self.score < other.score)) or \
            (person.symmetric and
             ((self.need < other.need) and \
             (self.score <= other.score)))

parser = argparse.ArgumentParser()

parser.add_argument('--format', metavar = 'fmt',
                    default = 'text',
                    choices = ['text','dot'],
                    help='{text, dot} (default text)')

parser.add_argument('--symmetric', dest='symmetric',
                    action='store_true')

parser.add_argument('--randomize', dest='randomize',
                    action='store_true')

parser.add_argument('input', metavar = 'file',
                    help = 'csv file')

args=parser.parse_args()

person.symmetric=args.symmetric
num_nodes = 0

nodes = {}

# input format is
#   int, number, number, number
with open(args.input,'r') as file:
    reader = csv.DictReader(file)
    for row in reader:
        ident = int(row['Id'])
        num_nodes += 1
        if args.randomize:
            needed=int(random.randrange(1,1500))
        else:
            needed=Decimal(row['Needed'])

        nodes[ident] = person(ident,
                              Decimal(row['FinancialNeed']),
                              Decimal(row['MedianRating']),
                              needed,
                              row['ApproveTravel'])

# we keep track of *inward* arcs, i.e. all nodes that
# dominate a current node. 
arcs = {}

for ident in nodes.iterkeys():
    arcs[ident] = set()
    for other in nodes.iterkeys():
        if nodes[ident] < nodes[other]:
            arcs[ident].add(other)

# we make copies here because our ranking algorithm is
# destructive

remaining = nodes.copy()
parents = copy.deepcopy(arcs)

levels = {}
count = 0

while len(remaining) != 0:

    # find nodes (people) not dominated by any remaining nodes.
    sources = set()
    for node in remaining:
        if len(parents[node]) == 0:
            sources.add(node)

    levels[count]=sources
    count += 1;

    # remove the most recently found set of sources from the
    # graph, and update the parent information of other nodes
    
    for node in sources:
        remaining.pop(node)
        for other_node in remaining:
            parents[other_node].discard(node)

# From here on, it's all about generating pretty output
total_cost = 0
if args.format == 'dot':
    print('digraph ranking {');
    for index in levels.iterkeys():
        level_cost=0
        print('subgraph { rank = "same";')
        for ident in levels[index]:
            level_cost += nodes[ident].cost
            print('{i:d} [shape="{shape:s}", label="{{{i:d}|{{{n:f}|{s:f}}}}}"];'. \
                  format(i=ident,
                         n=nodes[ident].need,
                         s=nodes[ident].score,
                         shape='Mrecord' if nodes[ident].approved =='Y' else 'record'))
        total_cost += level_cost;
        print 'level{0:d} [shape="record", label="{1:0.2f}|{2:0.2f}"];'.format(index,level_cost,total_cost)

        print('}')
        if index+1 in levels:
            print ('level{0:d} -> level{1:d};'.format(index,index+1))
            for current in levels[index]:
                for child in levels[index+1]:
                    if current in arcs[child]:
                        print("{0:d} -> {1:d};".format(current,child));


    print('}');

else:
    # format == text
    total_cost=0
    for index in levels.iterkeys():
        level_cost=0;
        for ident in levels[index]:
            level_cost += nodes[ident].cost
        total_cost+=level_cost
        print('{c:0.2f}\t{t:0.2f}\t{i:s}'. \
              format(c=level_cost,
                     t=total_cost,
                     i=",".join(str(x)
                              for x in sorted(levels[index]))))

import csv
import json
import sys

# export bursaries decisions as json

def read_file(name):
    dat = {}
    with open(name) as csvfile:
        reader = csv.DictReader(csvfile)
        fields = reader.fieldnames
        for row in reader:
            dat_row = {}
            for field in fields:
                
                if field in  ['ApproveAccom', 'ApproveDebCamp', 'ApproveTravel']:
                    if row[field] in ['Y', 'N', 'w', 'u', '']:
                        dat_row[field] = row[field]
                    else:
                        dat_row[field] = 'u'
                elif field in ['User', 'Name']:
                    dat_row[field] = row[field]

            dat[row['User']] = dat_row
    return (fields,dat)

(fields,dat) = read_file(sys.argv[1])

json.dump(dat, sys.stdout,
          indent=2, sort_keys=True,
          ensure_ascii=False)

                 
                            

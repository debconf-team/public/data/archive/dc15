* Preamble
  - In preliminary discussions we agreed that the amount of the
    request would not be a factor in the actual ranking, but that we
    would seperately sanity 
  - financial need will be combined with this ranking using the scheme
    proposed in mid:87wq3bml6k.fsf@maritornes.cs.unb.ca .

    Basically the idea is that nobody should lose out to someone with
    both a higher "contribution ranking" and a higher level of
    financial need.

* Proposed scale
** 5 "must fund"
   - If this person does not attend debconf, there will be significant
     negative impact for DebConf or Debian more generally.
** 4 "priority funding"
   - There are clear benefits to Debian or to DebConf of 
   this person attending debconf. 
   - This might be an accepted talk that seems particularly
     important.
** 3 "good initiative"
   - We should fund this person because they propose something
     interesting
   - Alternatively we should fund this person because they bring
     diversity to debian, or are a new contributor.
** 2 "good record"
   - this person has a strong record of contributing to Debian.
** 1 "OK"
   - if we have budget, I don't object to funding this request

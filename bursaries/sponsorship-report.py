#!/usr/bin/python3
# -*- coding: utf-8 -*-

import csv
import sys
import time
import json

fmt = "%Y-%m-%d %H:%M:%S"

with open(sys.argv[1]) as file:
    config=json.load(file)

conf_start=time.strptime(config['conf_start'],
                         "%Y-%m-%d").tm_yday

conf_nights = 0
camp_nights = 0
camp_people = 0
conf_people = 0
travel_people = 0
travel_amount = 0
with open(sys.argv[2],"r") as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        if row['ApproveTravel'] == 'Y':
            travel_people += 1
            travel_amount += int(row['Needed'])

        if row['ApproveAccom'] == 'Y':
            conf_people += 1
            arrival = time.strptime(row['Arrival'],fmt).tm_yday
            conf_arrival=max(arrival,conf_start)
            
            if row['ApproveDebCamp']=='Y':
                camp_people += 1
                row_camp_nights = max(conf_start - arrival, 0)
            else:
                row_camp_nights = 0
            
            departure = time.strptime(row['Departure'],fmt).tm_yday

            row_conf_nights = departure - conf_arrival

            conf_nights += row_conf_nights
            camp_nights += row_camp_nights

print("\tTravel\tCamp\tConf\tTotal")
print("people\t{travel:d}\t{camp:d}\t{conf:d}\t{total:d}". \
      format(travel=travel_people,camp=camp_people,
             conf=conf_people,total=conf_people))
print("nights\t\t{camp:d}\t{conf:d}\t{total:d}". \
      format(camp=camp_nights,
      conf=conf_nights,total=conf_nights+camp_nights))

camp_amount=camp_nights*config['camp_night_cost']
# if this extra EUR0.99 makes a difference, we have big problems
conf_amount=int(conf_nights*config['conf_night_cost'])
print("amount\t{travel:d}\t{camp:d}\t{conf:d}\t{total:d}". \
      format(travel=travel_amount,
             camp=camp_amount,
             conf=conf_amount,
             total=conf_amount+camp_amount+travel_amount))

    

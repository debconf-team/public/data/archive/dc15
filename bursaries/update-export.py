import csv
import sys

# merge new snapshot from
#  http://summit.debconf.org/debconf15/sponsorship/export

# the csv read is lazy/stupid/correct about converting types
def numerify (str):
    try:
        v = int(str)
    except:
        try:
            v = float(str)
        except:
            v = str
    return v

def read_file(name):
    dat = {}
    with open(name) as csvfile:
        reader = csv.DictReader(csvfile)
        fields = reader.fieldnames
        for row in reader:
            for field in fields:
                row[field] = numerify(row[field])
                
            dat[row['User']] = row
    return (fields,dat)

(old_fields,old_dat) = read_file(sys.argv[1])
(new_fields,new_dat) = read_file(sys.argv[2])

fields = ['Arrival', 'Departure']
for user in old_dat:
    if user in new_dat:
        for field in fields:
            old_dat[user][field] = new_dat[user][field]
                    


writer = csv.DictWriter(sys.stdout,old_fields,
                        quoting=csv.QUOTE_NONNUMERIC,
                        lineterminator='\n')
writer.writeheader()
for user in sorted(old_dat.keys()):
    writer.writerow(old_dat[user])
                            
                            

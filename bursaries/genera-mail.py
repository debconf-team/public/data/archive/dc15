#!/usr/bin/python3
# -*- coding: utf-8 -*-

# python3 genera-mail.py config.yaml data.csv
# for file in test-message.d/*; do /usr/lib/sendmail -ti < $file; done
#
# Based on a script by Ana Guerrero Lopez
#
# bits inspired by
#  https://git-tails.immerda.ch/whisperback/tree/whisperBack/encryption.py?h=feature/python3
#  http://www.physics.drexel.edu/~wking/code/python/send_pgp_mime
#  mailsender.py, by mitya57
#
# License: GPL3+?


import sys
import csv
import email
import time
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.encoders import encode_7or8bit
import email.header
import email.utils
import mailbox
import yaml
import gnupg

hash_algos = { 1: "md5",
               2: "sha1",
               3: "ripemd160",
               8: "sha256",
               9: "sha384",
               10: "sha512",
               11: "sha224" }

if sys.version_info.major < 3:
    raise RuntimeError("Only python 3 supported, sorry!")

with open(sys.argv[1],"r") as fp:
    config = yaml.safe_load(fp)

DC_MSG_ID = config['msg-id']

def generateEmailAddress(name, email_address):
    "format email address for message headers (allowing i18n)"

    if "," in name:
        name = name.replace('"', '')
        name = '"{0:s}"'.format(name)

    try:
        tmp = name.encode('us-ascii')
    except UnicodeError:
        name = str(email.header.Header(name, 'utf-8').encode())

    header = "{0:s} <{1:s}>".format(name, email_address)

    return header


def unicodify(obj, encoding='utf-8'):
    if isinstance(obj, str):
        if not isinstance(obj, str):
            obj = str(obj, encoding)
    return obj

mbox = mailbox.MH(DC_MSG_ID + ".d")
mbox.clear()
mbox.flush()

gpg = gnupg.GPG()

with open(sys.argv[2]) as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        body_text = config['body'] % row
        body = MIMEText(body_text, _subtype='plain')

        basetext = body.as_string().replace('\n', '\r\n')
        signature = gpg.sign(basetext, detach=True)
        if not signature.data:
            raise RuntimeError("gnupg failed to sign message")

        sig = MIMEApplication(_data=str(signature),
                              _subtype='pgp-signature; name="signature.asc"',
                              _encoder=encode_7or8bit)
        sig['Content-Description'] = 'signature'
        sig.set_charset('us-ascii')

        hash_index = int(signature.hash_algo)
        if hash_index in hash_algos:
            hash=hash_algos[hash_index]
        else:
            raise RuntimeError("Unknown hash index")

        msg = MIMEMultipart('signed', micalg='pgp-{0:s}'.format(hash),
                            protocol='application/pgp-signature')
        msg.attach(body)
        msg.attach(sig)

        # this is NOT and rfc2822 date. No idea why not.
        msg.set_unixfrom('From {0:s} {1:s}'. \
                         format(config.get('envelope-from') or 'nobody',
                                time.ctime()))

        msg['Subject'] = config['subject']
        msg['From'] = generateEmailAddress(*config['from'])
        msg['Reply-To'] = generateEmailAddress(*config['reply-to'])
        msg['To'] = generateEmailAddress(row['Name'], row['Email'])
        msg['Cc'] = generateEmailAddress(*config['cc'])
        if 'bcc' in config:
            msg['Bcc'] = generateEmailAddress(*config['bcc'])
        msg['Message-Id'] = email.utils.make_msgid(DC_MSG_ID)
        msg["User-Agent"] = "debconf python scripts"
        msg["Date"] = email.utils.formatdate(localtime=False)

        mbox.add(msg.as_string(True))

mbox.flush()
mbox.close()


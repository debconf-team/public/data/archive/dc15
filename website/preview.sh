#!/bin/sh
#
# preview.sh — preview DebConf websites
#
# Copyright © martin f. krafft <madduck@madduck.net>
# Released under the terms of the Artistic Licence 2.0
#
set -eu

LAUNCH_BROWSER=1

for arg in "$@"; do
  case "$arg" in
    (-n) LAUNCH_BROWSER=0;;
    (*.xhtml)
      if [ -n "${PAGE:-}" ]; then
        echo >&2 "W: Preview page already specified, skipping argument: $arg"
      else
        PAGE="$arg"
      fi
      ;;
    (*)
      if [ -d "$arg" ]; then
        if [ -n "${OUTDIR:-}" ]; then
          echo >&2 "W: Output directory already specified, skipping argument: $arg"
        else
          OUTDIR="$arg"
        fi
      else
        echo >&2 "W: Skipping unknown argument: $arg"
      fi
      ;;
  esac
  shift
done

: ${PAGE:=index.xhtml}
if [ -z "${OUTDIR:-}" ]; then
  OUTDIR=$(mktemp -p ${OUTDIR:-/tmp} -d www-dc15.XXXXXXXXXX)
  cleanup() { rm -rf $OUTDIR; trap - 0; }
  trap cleanup 0
  DO_CLEANUP=1
else
  DO_CLEANUP=0
fi

ttree --binmode=:utf8 -f ${0%/*}/ttreerc \
  --define rssfile=${0%/*}/dummy.rss \
  --define rssarchive=${0%/*}/dummy.rss \
  -a -s ${0%/*} -d $OUTDIR

echo "Files are in $OUTDIR ..."
if [ $LAUNCH_BROWSER = 1 ]; then
  echo "Now launching a sensible browser (-n prevents)..."
  sensible-browser "$OUTDIR/$PAGE"
fi

if [ "$DO_CLEANUP" = 1 ]; then
  trap : 2
  echo -n "Hit enter to remove temporary files and continue..."; read line

  cleanup
fi

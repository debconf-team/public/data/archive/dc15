function loadColor() {
  var d = new Date();
  if (d.getUTCHours() > 17 || d.getUTCHours() < 6) {
    document.body.className = "";
  } else {
    document.body.className = "contrast";
  }
}

function unhide(divID) {
	var item = document.getElementById(divID);
	if (item) {
		item.className=(item.className=='hidden')?'unhidden':'hidden';
	}
}

// Contrast cookie state
function switchContrast(element) {
  var contrast;
  var d = new Date();
  d.setTime(d.getTime() + (316*24*60*60*1000));
  var expires = "expires="+d.toGMTString();
  if(document.body.className != "contrast") {
      contrast="contrast";
  }
  else {
      contrast="";
  }
      document.body.className=contrast;
      document.cookie="contrast="+contrast+"; "+ expires +"=never; path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
}

function checkContrast() {
  var getContrast = getCookie("contrast");
  document.body.className = getContrast;

//  if (getContrast == ""){
//    loadColor();
//  } else {
//    document.body.className = getContrast;
//  }

}



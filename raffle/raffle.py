#/usr/bin/env python
"""
A simple raffle script for DebConf15

The code on this script was created by looking at several pygame
examples that are present on the internet. Among those, these two were
particularly helpful:

https://www.pygame.org/docs/tut/chimp/ChimpLineByLine.html
http://www.pygame.org/wiki/toggle_fullscreen

This code was written by Margarita Manterola, for DebConf15.

It is released to the public domain, i.e. CC0 or BOLA, or DWTFYW or any other
equivalent "license".
"""

import csv
import os
import pygame
from pygame.locals import *
import random
import time

if not pygame.font:
  print "No pygame.font"
  sys.exit(1)

WIDTH = 1024
HEIGHT = 768

LOADING_MESSAGE = "Loading DebConf attendees:"
FINISHED_MESSAGE = "attendees loaded. Click to start the raffle."
CSVFILE="attendees.csv"

def main():
    # Initialize Everything
    pygame.init()
    screen = pygame.display.set_mode((WIDTH,HEIGHT),pygame.FULLSCREEN)
    #screen = pygame.display.set_mode((WIDTH,HEIGHT))
    pygame.display.set_caption('DebConf Raffle')
    pygame.mouse.set_visible(0)

    # Create a white background
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((250, 250, 250))

    # Put text on the background, centered horizontally and vertically
    font = pygame.font.Font(None, 36)
    text = font.render(LOADING_MESSAGE, 1, (10, 10, 10))
    textpos = text.get_rect(x=50, y=10)
    background.blit(text, textpos)

    # Show the white background with the loading message
    screen.blit(background, (0, 0))
    pygame.display.flip()

    # Start the clock
    clock = pygame.time.Clock()

    # Open the file - Load attendees
    attendees = []
    rr = random.randrange
    i = 0
    with open(CSVFILE) as opened_file:
        reader = csv.DictReader(opened_file)
        for row in reader:
            clock.tick(60)
            attendees.append((row["id"], row["name"].decode('utf8')))
            i += 1
            # Draw a square for each attendee
            background.fill((rr(0,256),rr(0,256),rr(0,256)),(rr(0,WIDTH),rr(40,HEIGHT),32,32))

            # White square behind the counter
            background.fill((250,250,250),(400,10,60,30))
            # Counter text
            counter = font.render(str(i), 1, (10, 10, 10))
            counterpos = counter.get_rect(x=400, y=10)
            background.blit(counter, counterpos)

            screen.blit(background, (0, 0))
            pygame.display.flip()

    text = font.render(FINISHED_MESSAGE, 1, (10, 10, 10))
    textpos = text.get_rect(x=450, y=10)
    background.blit(text, textpos)
    screen.blit(background, (0, 0))
    pygame.display.flip()

    # Main Loop
    while 1:
        clock.tick(25)
        # Handle Input Events
        for event in pygame.event.get():
            if event.type == QUIT:
                return
            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                return
            elif event.type == MOUSEBUTTONDOWN:
                do_raffle(background, screen, attendees)
            elif event.type == KEYDOWN and event.key == K_r:
                do_raffle(background, screen, attendees)

def do_raffle(background, screen,attendees):
    background.fill((250, 250, 250))
    screen.blit(background, (0, 0))
    pygame.display.flip()
    rr = random.randrange
    font = pygame.font.SysFont("FreeSerif", 60)
    # Countdown
    for i in xrange(3):
        background.fill((rr(0,256),rr(0,256),rr(0,256)),
                        (50*(i+1),50*(i+1),WIDTH-100*(i+1),HEIGHT-100*(i+1)))

        counter = font.render(str(3-i), 1, (10, 10, 10))
        counterpos = counter.get_rect(centerx=WIDTH/2, centery=HEIGHT/2)
        background.blit(counter, counterpos)

        screen.blit(background, (0, 0))
        pygame.display.flip()
        time.sleep(1)
    # Winner
    background.fill((0,0,0),(0,0,WIDTH,HEIGHT))

    w = rr(len(attendees))
    font = pygame.font.SysFont("FreeSerif", 80)
    label = font.render("And the winner is...", 1, (250, 250, 250))
    labelpos = label.get_rect(centerx=WIDTH/2, centery=HEIGHT/2-150)
    background.blit(label, labelpos)
    screen.blit(background, (0, 0))
    pygame.display.flip()

    time.sleep(1)

    names = attendees[w][1].split()
    for i, name in enumerate(names):
        winner = font.render(u"%s" % name, 1, (250, 250, 250))
        winnerpos = winner.get_rect(centerx=WIDTH/2, centery=HEIGHT/2 + i*80)
        background.blit(winner, winnerpos)
        screen.blit(background, (0, 0))
        pygame.display.flip()
        time.sleep(0.5)

#this calls the 'main' function when this script is executed
if __name__ == '__main__': main()

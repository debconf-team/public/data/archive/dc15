% DebConf15 in Heidelberg
% Michael Banck, Rene Engelhard, Martin Krafft, Margarita Manterola, ... and many others who couldn't be here
% 2014-08-30

DebConf15 in Heidelberg
=======================

1. Introduction
2. Bid-Process Recap
3. Location and Getting There
4. Venue
5. Conference and Timing

Bid-Process Recap
=================

* Prospective Germany team convened at DebConf13 
* Local Team of 15-20 people
* Decided to look Germany-wide, not particular locations
* Decided to look at venues, not particular cities

Bid-Process Recap
=================

* Scouted 75+ venues: https://wiki.debconf.org/wiki/DebConf15/Germany/VenueScouting
* Long-list of 9, mediumlist of 5, shortlist of 3 cities (Munich, Heidelberg, Treuchtlingen)
* Visited all three shortlist cities/proposed venues
* Dropped Treuchtlingen due to lack of suitable venue
* Submitted Bid with two-venue strategy: 
	1. Hochschule München/Haus International
	2. Heidelberg International
* Post-bid settled on Heidelberg
* Kick-off team-meeting at the Heidelberg venue in May (18 people)

Local Team
==========

![Local Team Location](images/LocalTeam.png)

Local Team
=========

* 12-37 people, depending on how you count
	* DebConf13 meeting: 18 people
	* Bid: 17 people
	* Kick-Off meeting: 18 people
* Union of all three: 37 people
* Present twice out of three: 12 people

Where we live
=============

![Local team location](images/LocalTeam.jpg)

Location
========

* Germany

Location
========

* Germany
	* It's in the middle of Europe

Heidelberg
==========

![Picture of Old City of Heidelberg](images/1024px-Heidelberg_Castle_and_Bridge.jpg)

Heidelberg
==========

* Divided by Neckar river, hills around
* Old Celtic (ca. 500 B.C.) and Roman forts (ca. 50-250 A.D.), medieval town (5th century) with one of Europe's oldest universities 
* University tagline is "Zukunft. Seit 1386"
* 150k Inhabitants
* Popular tourist location, 3.5 million visitors per year
* Pretty "Altstadt" with Heidelberg castle towering over it
* UNESCO World Heritage

Heidelberg venue location
=========================

![Picture of Heidelberg](images/1024px-Heidelberg-luftbild-aerial-photograph.jpg)

Heidelberg venue location
=========================

* Heidelberg International youth hostel
* Located a few km from city center
* Next to zoo, university, river
* Bus stop in front of venue


Transportation
==============

![Important Airports in Germany](images/ImportantAirports.png)

Transportation
==============

* 10 minutes from main train station via bus
* Bus stop in front of venue
* By air
	* 1:00-1:30 from FRA by train (80 km) 
	* 1:45-2:00 from STR/FKB (more regional) by train (80 km)
* By train
	* 0:20 from Mannheim Hbf, 0:45 from Karlsruhe Hbf, major high-speed train hubs
	* 3:30-3:45 from Paris or Zurich by high-speed train

Airports
========
![International airports nearby](images/ImportantAirports.jpg)

Venue
=====

-------

![Picture of Venue](images/1.jpg)

-------

![Picture of Venue](images/2.jpg)

-------

![Picture of Venue](images/3.jpg)

-------

![Picture of Venue](images/4.jpg)

-------

![View of the nearby Zoo](images/5.jpg)

-------

![Venue's Reception Area](images/6.jpg)

-------

![Venue's Cafeteria](images/7.jpg)

-------

![Venue's Terrace](images/8.jpg)

-------

![Sports Area](images/9.jpg)

-------

![Sample room](images/10.jpg)

Venue
=====

* All-in-one-place, under one roof
	* i.e. no separate night hack-lab, no walk to/from food
* Exclusive access (except during Debcamp)
* Capacity
	* number of beds available: ca. 400, 2-6 beds/room
	* talk rooms: 250, 120, 40, and 8 rooms with 10–20 people
	* any number of the smaller above rooms, but we rather think there
	  will be hack areas.
* Venue features
	* Internet
		* 1Gbps fibre link to the university (with mirror)
	* food arrangements
	* C&W party
	* Child care
	* DISCO
* Contract signed

Conference Plans
================

* Debcamp
* Open Weekend
* Conference dinner and/or BBQ
* Number of concurrent tracks (?)
	* number of (talk?) rooms, and projected amount of time slots?
* Additional sponsorship ideas
	* job wall/fair
	* sponsor booths
* Poster session
* Music: disco, instruments, band, karaoke, …
* Hardware corner

Timing
======

* DebConf: August 15-22 (Sat–Sat)
* Debcamp: August 8-14 (Sat–Fri)
* DebConf starts on a Saturday, if possible arrive on Friday
* Adjacent FLOSS Events (TBC)
	* CCC Camp, (near) Berlin
	* FrOSCon, (near) Cologne
	* Debian UK BBQ, Cambridge
* Call for Sprints during Debcamp
* Timeline
	* Call for papers: January
	* Preliminary schedule and Registraton in March
	* Sponsorship from March onwards

Call for help
=============

* Locals to Heidelberg
* Heidelberg University Contacts
* Ideas for day trips
* Sponsorship team

DebConf15
=========

See you at DebConf15 in Heidelberg!

* Website: http://debconf15.debconf.org
* Mailing list: debconf-team@lists.debconf.org
* IRC channel: #debconf-team on irc.debian.org
* Twitter: @DebConf
